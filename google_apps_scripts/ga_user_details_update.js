/*
 * ga_user_details_update
 * By Nich Young
 *
 * Script to update Google Apps user database with work and mobile phone
 * from a Google Spreadsheet.
 *
 * Update SPREADSHEET_ID and domain name from example.com to run
 *
*/

function updateGAUsers() {
  var SPREADSHEET_ID = "GOOGLE_APPS_SPREADSHEET_ID"

  var gaUsers = getGAUsers()
  var ssUsers = getSSUsers(SPREADSHEET_ID)
  
  for (var key in ssUsers) {
    if (gaUsers[key] != undefined) {
      var gaUser = gaUsers[key]
      var ssUser = ssUsers[key]
      var updateP = false

      if ( gaUser["phones"] != undefined ) {
        gaUW = findPhonebyType(gaUser["phones"], "work")
        gaUM = findPhonebyType(gaUser["phones"], "mobile")
        ssUW = ssUser["work"]
        ssUM = ssUser["mobile"]
        if ( (ssUW != "" && ssUW != gaUW) || ( ssUM != "" && ssUM != gaUM) ) {
          updateP = true
        }
      } else {
        updateP = true
      }
      
      if (updateP) {
        var phonePatch = buildPhoneHash(ssUser)
        Logger.log('Update %s: %s', key, phonePatch)
        AdminDirectory.Users.patch(phonePatch, key)
      }
    }
  }
}

function findPhonebyType(phones, typePhone) {
  for (i in phones) {
    if ( phones[i]["type"] == typePhone ) {
      return phones[i]["value"]
    }
  }
}

function buildPhoneHash(ssUser) {
  var phone = {}
  if ( ssUser["work"] != "" && ssUser["mobile"] != "" ) {
    phone = {"phones" : [{"type" : "work", "value" : ssUser["work"]}, {"type" : "mobile", "value" : ssUser["mobile"]}] }
  } else if (ssUser["work"] != "") {
    phone = {"phones" : [{"type" : "work", "value" : ssUser["work"]}] }
  } else {
    phone = {"phones" : [{"type" : "mobile", "value" : ssUser["mobile"]}] }
  }
  return phone
}

function getGAUsers() {
  var pageToken, page;
  var userhash = {}
  do {
    page = AdminDirectory.Users.list({
      domain: 'example.com',
      orderBy: 'givenName',
      maxResults: 100,
      query: 'isSuspended=false',
      pageToken: pageToken
    });
    var users = page.users;
    if (users) {
      for (var i = 0; i < users.length; i++) {
        var user = users[i];
        userhash[user.primaryEmail] = user
      }
    }
    pageToken = page.nextPageToken;
  } while (pageToken);
  
  return userhash
}

function getSSUsers (ssId) {
  var ss = SpreadsheetApp.openById(ssId)
  var aduser = {}
  usersheet = ss.getSheetByName("users")
  userdata = usersheet.getDataRange().getValues();
  for ( i in userdata ) {
    if ( userdata[i][1] != "" || userdata[i][2] != "" ) {
      aduser[userdata[i][0]] = {"email" : userdata[i][0], "work" : userdata[i][1], "mobile" : userdata[i][2]}
    }
  }
  return aduser
}
