# Get-WmiObject -Class win32_service -Property name,pathname | Where-Object { $_.Pathname -match '(^.*\s+.*\.exe$|^[^"].*\s+.*\.exe)' } | fl name,pathname

# Get vulnerable services
$vulnerable = Get-WmiObject -Class win32_service -Property name,pathname | Where-Object { $_.Pathname -match '(^.*\s+.*\.exe$|^[^"].*\s+.*\.exe)' }

foreach ( $v in $vulnerable ) {
  $fixedPathName = $v.PathName -replace '(^.*\s+.*\.exe)', '"$1"'

  Write ("Fixing: " + $v.Name  + ":" + $v.PathName + ":" + $fixedPathName )
  $regLoc = ('HKLM:\SYSTEM\CurrentControlSet\Services\' + $v.Name)

  try {
    Set-ItemProperty -Path $regLoc -Name ImagePath -Value $fixedPathName -Verbose
  } Catch {
    Write-Error ("Unable to fix " + $v.Name)
  }
}
